module.exports = {
  en: {
    services: {
      columns: [{
        img: "../../assets/images/services/jitsi.webp",
        icon: "video",
        title: "Meet",
        subtitle: "Video conference",
        button: "Create a room",
        link: "https://meet.altertek.org/"
      },
      {
        img: "../../assets/images/services/matrix.png",
        icon: "comments",
        title: "Matrix",
        subtitle: "Messaging",
        button: "Create an account",
        link: "https://matrix.to/#/#altertek:altertek.org"
      },
      {
        img: "../../assets/images/services/peertube.webp",
        icon: "play",
        title: "Video",
        subtitle: "Video-sharing",
        button: "Browse the videos",
        link: "https://video.altertek.org"
      }]
    }
  },
  fr: {
    services: {
      columns: [{
        img: "../../assets/images/services/jitsi.webp",
        icon: "video",
        title: "Meet",
        subtitle: "Conférence vidéo",
        button: "Créer un salon",
        link: "https://meet.altertek.org/"
      },
      {
        icon: "comments",
        img: "../../assets/images/services/matrix.png",
        title: "Matrix",
        subtitle: "Messagerie instantanée",
        button: "Créer un compte",
        link: "https://matrix.to/#/#altertek:altertek.org"
      },
      {
        img: "../../assets/images/services/peertube.webp",
        icon: "play",
        title: "Video",
        subtitle: "Hébergement de vidéos",
        button: "Parcourir les vidéos",
        link: "https://video.altertek.org"
      }]
    }
  }
}
