module.exports = {
  en: {
    software: [{
      img: "../../assets/images/software/whatimupto.png",
      title: "Whatimupto",
      subtitle: "Linktree alternative",
      button: "See the project",
      link: "https://github.com/altertek/whatimupto"
    }]

  },
  fr: {
    software: [{
      img: "../../assets/images/software/whatimupto.png",
      title: "Whatimupto",
      subtitle: "Alternative à Linktree",
      button: "Voir le projet",
      link: "https://github.com/altertek/whatimupto"
    }]
  }
}
