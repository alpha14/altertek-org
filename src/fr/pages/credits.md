---
layout: 'layouts/simple-page.njk'
title: Crédits
pageName: credits
permalink: /{{ locale }}/{{ pageName }}/
---

# {{ title }}

Conception: Altertek

Contenu sous Creative Commons BY-SA.

Liste des projets open source utilisés pour ce site:
- [Eleventy](https://www.11ty.dev/)
- [HTMLminifier](https://github.com/kangax/html-minifier)
- [PurifyCSS](https://github.com/purifycss/purifycss)
- [Bootstrap](https://getbootstrap.com)
- [Jquery](https://jquery.com/)
- [Fork-Awesome](https://forkaweso.me/Fork-Awesome/)

Accessibilité: Valide (voir le [test Wave](https://wave.webaim.org/report#/https://altertek.org/))

Vous voulez contribuer? Consultez notre [page de contributions](/{{ locale }}/contribute)
